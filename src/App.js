import React from 'react';

import AppBar from './containers/AppBar';
import Drawer from './containers/Drawer';

import routes from './navigations/index'

function App() {
  const [openDrawer, setOpenDrawer] = React.useState(false)
  return (
    <div>
      <AppBar onClick={() => setOpenDrawer(true)} />
      <Drawer {...{routes, openDrawer, setOpenDrawer}}/>
    </div>
  );
}

export default App;
