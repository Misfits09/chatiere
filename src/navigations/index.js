import MailIcon from '@mui/icons-material/Mail';

const routes = {
    "main": [
        {
            link: '/instruction',
            name: 'Instruction',
            icon: <MailIcon />
        },
        {
            link: '/reporting',
            name: 'Rapport',
            icon: <MailIcon />
        }
    ],
    "appendix": [
        {
            link: '/help',
            name: "Guide d'utilisation",
            icon: <MailIcon />
        },
        {
            link: '/contact',
            name: 'contact',
            icon: <MailIcon />
        }
    ]
};

export default routes; 